package com.devcamp.api.rainbowapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.rainbowapi.services.RainbowService;

@CrossOrigin
@RestController
public class RainbowController {
    @Autowired
    RainbowService rainbowService;

    @GetMapping("/rainbow-request-query")
    public ArrayList<String> filterRainbow(@RequestParam(value="q", defaultValue = "") String keyword) {
        ArrayList<String> rainbows = new ArrayList<>();

        //RainbowService rainbowService = new RainbowService();
        rainbows = rainbowService.filterRainbows(keyword);

        return rainbows;
    }

    @GetMapping("/rainbow-request-param/{ind}")
    public String getRainbow(@PathVariable(name="ind") int index) {
        return rainbowService.getRainbow(index);        
    }
}
